import 'package:flutter/material.dart';
import 'package:daligo_customer_app/functions/token_lib.dart';
import 'package:daligo_customer_app/pages/page_index.dart';
import 'package:daligo_customer_app/pages/page_login.dart';

class MiddlewareLoginCheck {
  void check(BuildContext context) async {
    String? memberToken = await TokenLib.getMemberToken();

    if (memberToken == null) {
      Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (BuildContext context) => const PageLogin()), (route) => false);
    } else {
      Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (BuildContext context) => const PageIndex()), (route) => false);
    }
  }
}
