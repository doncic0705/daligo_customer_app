import 'package:bot_toast/bot_toast.dart';
import 'package:daligo_customer_app/components/common/component_appbar_actions.dart';
import 'package:daligo_customer_app/components/common/component_drawer.dart';
import 'package:daligo_customer_app/components/common/component_notification.dart';
import 'package:daligo_customer_app/config/config_color.dart';
import 'package:daligo_customer_app/config/config_size.dart';
import 'package:daligo_customer_app/enums/enum_size.dart';
import 'package:daligo_customer_app/model/remain_amount_result.dart';
import 'package:daligo_customer_app/pages/page_main.dart';
import 'package:flutter/material.dart';
import 'package:daligo_customer_app/components/common/component_custom_loading.dart';
import 'package:daligo_customer_app/components/common/component_margin_vertical.dart';
import 'package:daligo_customer_app/components/common/component_text_btn.dart';
import 'package:daligo_customer_app/config/config_style.dart';
import 'package:daligo_customer_app/model/remain_price_request.dart';
import 'package:daligo_customer_app/repository/repo_amount.dart';
import 'package:intl/intl.dart';

class PageRemainPrice extends StatefulWidget {
  const PageRemainPrice({super.key});


  @override
  State<PageRemainPrice> createState() => _PageRemainPriceState();
}

class _PageRemainPriceState extends State<PageRemainPrice> {
  num remainPrice = 0;

  var maskWonFormatter = NumberFormat.currency(locale: "ko_KR", symbol: "");

  Future<void> _loadProfileData() async {
    var repository = RepoAmount();
    RemainAmountResult result = await repository.getRemainPrice();

    setState(() {
      remainPrice = result.data.remainPrice;
    });
  }

  @override
  void initState() {
    super.initState();
    _loadProfileData();
  }

  num price = 0;
  num resultPrice = 0;

  _incrementCounter(double amount) {
    setState(() {
      this.price += amount;
    });
  }

  _resetCounter() {
    setState(() {
      this.price = 0;
    });
  }

  Future<void> _doRemainPrice() async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoAmount().doCharge(price).then((res) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: true,
        title: '금액 충전 완료',
        subTitle: '금액 충전이 완료되었습니다.',
      ).call();

      Navigator.pop(context);
    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: false,
        title: '금액 충전 실패',
        subTitle: '고객센터로 문의주세요.',
      ).call();
    });
  }

  _paymentAmountButton(double value) {
    return Container(
      width: MediaQuery.of(context).size.width * 0.29,
      child: ComponentTextBtn(
          '+' + maskWonFormatter.format(value),
              () { _incrementCounter(value); }
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ComponentAppbarActions(title: "금액 충전"),
      body: _buildBody(context),
      drawer: ComponentDrawer(),
    );
  }

  Widget _buildBody(BuildContext context) {
    return ListView(
      children: [
        Container(
          padding: EdgeInsets.all(20),
          child: Container(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Row(
                  children: [
                    const Row(
                      children: [
                        Icon(Icons.attach_money, size: 30),
                      ],
                    ),
                    Container(
                      margin: EdgeInsets.only(left: 10),
                      child: Text("충전금액",
                        style: TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.w700,
                        ),
                      ),
                    ),
                  ],
                ),
                const ComponentMarginVertical(enumSize: EnumSize.mid),
                Container(
                  padding: EdgeInsets.only(left: 20),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(buttonRadius),
                    border: Border.all(color: colorLightGray),
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                          child: Text(
                            maskWonFormatter.format(price),
                            style: TextStyle(fontSize: fontSizeBig),
                          )),
                      IconButton(
                        onPressed: () => _resetCounter(),
                        icon: Icon(Icons.cancel, color: colorRed),
                      ),
                    ],
                  ),
                ),
                ComponentMarginVertical(enumSize: EnumSize.mid),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    _paymentAmountButton(1000),
                    _paymentAmountButton(5000),
                    _paymentAmountButton(10000),
                  ],
                ),
                const ComponentMarginVertical(),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text("충전 후 금액",
                      style: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.w700,
                      ),
                    ),
                    Text(maskWonFormatter.format(price + remainPrice) + "원",
                      style: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.w700,
                      ),
                    ),
                  ],
                ),
                const ComponentMarginVertical(enumSize: EnumSize.big),
                Row(
                  children: [
                    Icon(Icons.credit_card, size: 30),
                    const ComponentMarginVertical(),
                    Container(
                      margin: EdgeInsets.only(left: 10),
                      child: Text("결제 전 주의사항",
                        style: TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.w700,
                        ),
                      ),
                    )
                  ],
                ),
                const ComponentMarginVertical(enumSize: EnumSize.mid),
                Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text("※ 자동 충전 금액은 최소 1,000캐시 ~ 최대 50,000캐시까지 1천 캐시 단위로 입력 가능합니다.",),
                      const ComponentMarginVertical(),
                      Text("※ 최초 1회 결제 이후에는 회원님이 설정한 조건대로 결제가 진행됩니다."),
                      const ComponentMarginVertical(),
                      Text("※ 결제 카드를 변경할 시에는 해지 후 카드를 재설정 하셔야 변경이 가능합니다."),
                    ],
                  ),
                ),
                const ComponentMarginVertical(enumSize: EnumSize.big),
                Container(
                  child: ComponentTextBtn('금액 충전', () {
                    RemainPriceRequest remainPriceRequest = RemainPriceRequest(price);
                    _doRemainPrice();
                    Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => const PageMain()));
                  }),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}


