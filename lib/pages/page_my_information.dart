import 'package:daligo_customer_app/components/common/component_appbar_popup.dart';
import 'package:daligo_customer_app/model/amount_charging_request.dart';
import 'package:daligo_customer_app/model/update_password_request.dart';
import 'package:daligo_customer_app/model/update_request.dart';
import 'package:daligo_customer_app/pages/page_member_update.dart';
import 'package:daligo_customer_app/pages/page_password_update.dart';
import 'package:daligo_customer_app/pages/page_remain_price.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import '../components/common/component_margin_vertical.dart';
import '../components/common/component_text_btn.dart';

class PageMyInformation extends StatefulWidget {
  const PageMyInformation({super.key});

  @override
  State<PageMyInformation> createState() => _PageMyInformationState();

  @override
  Size get preferredSize {
    return const Size.fromHeight(45);
  }
}

class _PageMyInformationState extends State<PageMyInformation> {
  final _formKey = GlobalKey<FormBuilderState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar:
          ComponentAppbarPopup(
            title: "나의 정보",
          ),
      body: Container(
        padding: EdgeInsets.all(20),
        child: Column(
          children: [
            Container(
              padding: EdgeInsets.all(15),
              decoration: BoxDecoration(
                border: Border.all(
                  width: 1,
                  color: const Color.fromRGBO(204, 204, 204, 100),
                ),
                borderRadius: BorderRadius.circular(4),
              ),
              child: const Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text("이름",
                        style: TextStyle(
                          fontSize: 22,
                        ),
                      ),
                      Text("고달리",
                        style: TextStyle(
                          fontWeight: FontWeight.w700,
                          fontSize: 22,
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text("연락처",
                        style: TextStyle(
                          fontSize: 22,
                        ),
                      ),
                      Text("010-1234-5678",
                        style: TextStyle(
                          fontSize: 22,
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text("보유금액",
                      style: TextStyle(
                        fontSize: 22,
                        ),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text("50원",
                            style: TextStyle(
                              fontSize: 22,
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text("생년월일",
                        style: TextStyle(
                          fontSize: 22,
                        ),
                      ),
                      Text("1997.11.08",
                        style: TextStyle(
                          fontSize: 22,
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            const ComponentMarginVertical(),
            Container(
              width: MediaQuery.of(context).size.width,
              child: ComponentTextBtn('회원 수정', () => Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => const PageMemberUpdate())),),
            ),
            SizedBox(
              height: 15,
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              child: ComponentTextBtn('충전 하기', () => Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) =>  const PageRemainPrice())),),
            ),
            SizedBox(
              height: 15,
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              child: ComponentTextBtn('비밀번호 수정', () => Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => const PagePasswordUpdate())),),
            ),
          ],
        ),
      ),
    );
  }
}