import 'package:daligo_customer_app/components/common/component_appbar_popup.dart';
import 'package:daligo_customer_app/components/common/component_drawer.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import '../components/common/component_margin_vertical.dart';
import '../components/common/component_text_btn.dart';

class PageFindUsernameTwo extends StatefulWidget {
  const PageFindUsernameTwo(
      {super.key,});

  @override
  State<PageFindUsernameTwo> createState() => _PageFindUsernameTwoState();

  @override
  Size get preferredSize {
    return const Size.fromHeight(45);
  }
}

class _PageFindUsernameTwoState extends State<PageFindUsernameTwo> {
  final _formKey = GlobalKey<FormBuilderState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar:
      ComponentAppbarPopup(
        title: "아이디 찾기",
      ),
      body: _buildBody(context),
      drawer: ComponentDrawer(),
    );
  }

  Widget _buildBody(BuildContext context) {
    return ListView(
      children: [
        const ComponentMarginVertical(),
        Stack(
          children: [
            Container(
              padding: const EdgeInsets.only(
                bottom: 10,
                left: 20,
                right: 20,
              ),
              child: FormBuilder(
                key: _formKey,
                autovalidateMode: AutovalidateMode.disabled,
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    SizedBox(
                      height: 50,
                    ),
                    Container(
                      child: Text(
                        "고객님의 정보와 일치하는 아이디입니다.",
                        style: TextStyle(
                          fontSize: 20,
                        ),
                      ),
                    ),
                    const ComponentMarginVertical(),
                    Container(
                      margin: EdgeInsets.only(left: 1, right: 20),
                      child: Text(
                        "daligo1234",
                        style: TextStyle(
                          fontSize: 20,
                        ),
                      ),
                    ),
                    const ComponentMarginVertical(),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          width: MediaQuery.of(context).size.width * 0.45,
                          child: ComponentTextBtn('로그인하기', () {}),
                        ),
                        Container(
                          width: MediaQuery.of(context).size.width * 0.45,
                          child: ComponentTextBtn('비밀번호 찾기', () {}),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ],
    );
  }
}
