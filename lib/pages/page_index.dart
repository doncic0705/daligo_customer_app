import 'package:flutter/material.dart';
import 'package:daligo_customer_app/pages/page_ing_kickboard.dart';
import 'package:daligo_customer_app/pages/page_main.dart';
import 'package:daligo_customer_app/pages/page_my.dart';
import 'package:daligo_customer_app/pages/page_near_kickboard.dart';

class PageIndex extends StatefulWidget {
  const PageIndex({Key? key}) : super(key: key);

  @override
  State<PageIndex> createState() => _PageIndexState();
}

class _PageIndexState extends State<PageIndex> {
  int _selectedIndex = 0; // 기본화면을 HOME으로 설정

  final List<BottomNavigationBarItem> _navItems = [
    const BottomNavigationBarItem(
      icon: Icon(Icons.home),
      label: 'HOME',
    ),
    const BottomNavigationBarItem(
      icon: Icon(Icons.home),
      label: '근처킥보드',
    ),
    const BottomNavigationBarItem(
      icon: Icon(Icons.business),
      label: '이용중킥보드',
    ),
    const BottomNavigationBarItem(
      icon: Icon(Icons.account_circle),
      label: '마이페이지',
    ),
  ];

  final List<Widget> _widgetPages = [
    const PageMain(),
    const PageNearKickboard(),
    const PageIngKickboard(),
    const PageMy(),
  ];

  void _onItemTap(int index) {
    setState(() {
      _selectedIndex = index; // 누른 걸로 바뀜
    });
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea( // padding을 필요할 때만 넣음
        child: _widgetPages.elementAt(_selectedIndex),
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: _navItems,
        currentIndex: _selectedIndex,
        onTap: _onItemTap,
        unselectedItemColor: Colors.black,
        selectedItemColor: Colors.amber[800],
      ),
    );
  }
}
