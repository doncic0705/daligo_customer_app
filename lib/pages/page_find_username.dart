import 'package:bot_toast/bot_toast.dart';
import 'package:daligo_customer_app/components/common/component_appbar_popup.dart';
import 'package:daligo_customer_app/components/common/component_custom_loading.dart';
import 'package:daligo_customer_app/components/common/component_notification.dart';
import 'package:daligo_customer_app/config/config_color.dart';
import 'package:daligo_customer_app/enums/enum_size.dart';
import 'package:daligo_customer_app/model/auth_check_request.dart';
import 'package:daligo_customer_app/model/find_username_request.dart';
import 'package:daligo_customer_app/pages/page_find_username_two.dart';
import 'package:daligo_customer_app/repository/repo_member.dart';
import 'package:daligo_customer_app/repository/repo_phone_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';

import '../components/common/component_margin_vertical.dart';
import '../components/common/component_text_btn.dart';
import '../config/config_form_validator.dart';
import '../config/config_style.dart';
import '../model/auth_send_request.dart';
import '../model/login_request.dart';
import '../styles/style_form_decoration.dart';

class PageFindUsername extends StatefulWidget {
  const PageFindUsername({super.key});

  @override
  State<PageFindUsername> createState() => _PageFindUsernameState();

  @override
  Size get preferredSize {
    return const Size.fromHeight(45);
  }
}

class _PageFindUsernameState extends State<PageFindUsername> {
  final _formKey = GlobalKey<FormBuilderState>();

  var maskPhoneNumber = MaskTextInputFormatter(
      mask: '###-####-####',
      filter: { "#": RegExp(r'[0-9]') },
      type: MaskAutoCompletionType.lazy
  );

  String _authSendBtnName = '인증번호 전송';
  bool _isAuthNumSend = false; // 인증번호 보냈냐??
  bool _isAuthNumSuccess = false; // 인증 완료 했냐??

  Future<void> _doAuthSend(AuthSendRequest request) async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoPhoneAuth().doSend(request).then((res) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: true,
        title: '인증번호 전송',
        subTitle: '인증번호가 ${request.phoneNumber} 으로 전송되었습니다.',
      ).call();

      setState(() {
        _authSendBtnName = '인증번호 재전송';
        _isAuthNumSend = res.isSuccess; // 보냈냐를 true로 바꿔줌
      });
    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: false,
        title: '인증번호 전송 실패',
        subTitle: '고객센터로 문의하세요.',
      ).call();
    });
  }

  Future<void> _doAuthCheck(AuthCheckRequest request) async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoPhoneAuth().doCheck(request).then((res) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: true,
        title: '인증번호 확인 완료',
        subTitle: '인증번호 확인이 완료되었습니다.',
      ).call();

      setState(() {
        _isAuthNumSuccess = res.isSuccess;
      });
    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: false,
        title: '인증번호 확인 실패',
        subTitle: '인증번호를 정확히 입력해주세요.',
      ).call();
    });
  }

  Future<void> _findUsername(FindUsernameRequest findUsernameRequest) async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoMember().findUsername(findUsernameRequest).then((res) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: true,
        title: '아이디 찾기 완료',
        subTitle: '아이디를 확인하시려면 다음 버튼을 눌러주세.',
      ).call();

      Navigator.pushAndRemoveUntil( // 뒤로가기가 안됌
          context,
          MaterialPageRoute(
              builder: (BuildContext context) => const PageFindUsernameTwo()),
              (route) => false);
    }).catchError((err) {
      BotToast.closeAllLoading();


      ComponentNotification(
        success: false,
        title: '아이디 찾기 실패',
        subTitle: '입력값을 확인 해주세요.',
      ).call();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar:
          ComponentAppbarPopup(
            title: "아이디 찾기",
          ),
      body: _buildBody(context),
    );
  }

  Widget _buildBody(BuildContext context) {
    return ListView(
      children: [
        const ComponentMarginVertical(),
        Stack(
          children: [
            Container(
              padding: const EdgeInsets.only(
                bottom: 10,
                left: 20,
                right: 20,
              ),
              child: FormBuilder(
                key: _formKey,
                autovalidateMode: AutovalidateMode.disabled,
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Container(
                      decoration: formBoxDecoration,
                      padding: bodyPaddingAll,
                      child: FormBuilderTextField(
                        name: 'name',
                        decoration: StyleFormDecoration().getInputDecoration(
                            '이름'),
                        maxLength: 20,
                        keyboardType: TextInputType.text,
                        validator: FormBuilderValidators.compose([
                          FormBuilderValidators.required(
                              errorText: formErrorRequired),
                          FormBuilderValidators.minLength(
                              2, errorText: formErrorMinLength(2)),
                          FormBuilderValidators.maxLength(
                              20, errorText: formErrorMaxLength(20)),
                        ]),
                      ),
                    ),
                    const ComponentMarginVertical(),
                    Container(
                      decoration: formBoxDecoration,
                      padding: bodyPaddingAll,
                      child: FormBuilderTextField(
                        name: 'phoneNumber',
                        decoration: StyleFormDecoration().getInputDecoration(
                            '연락처'),
                        maxLength: 13,
                        inputFormatters: [maskPhoneNumber],
                        keyboardType: TextInputType.text,
                        validator: FormBuilderValidators.compose([
                          FormBuilderValidators.required(
                              errorText: formErrorRequired),
                          FormBuilderValidators.minLength(
                              13, errorText: formErrorMinLength(13)),
                          FormBuilderValidators.maxLength(
                              13, errorText: formErrorMaxLength(13)),
                        ]),
                        enabled: !_isAuthNumSuccess,
                      ),
                    ),
                    !_isAuthNumSuccess
                        ? Container(
                      child: ComponentTextBtn(
                        '$_authSendBtnName',
                            () {
                          String phoneNumberText = _formKey
                              .currentState!.fields['phoneNumber']!.value;
                          if (phoneNumberText.length == 13) {
                            AuthSendRequest authSendRequest = AuthSendRequest(
                              phoneNumberText,
                            );
                            _doAuthSend(authSendRequest); // 인증번호를 전송해줌.
                          }
                        },
                        bgColor: colorSecondary,
                        borderColor: colorSecondary,
                      ),
                    ) // true일 때 이걸 띄움
                        : Container(), // false 일 때 컨테이너만 띄움
                    const ComponentMarginVertical(),
                    _isAuthNumSend && !_isAuthNumSuccess // 인증번호를 전송했는데 인증 완료가 안됐을 때
                        ? Container(
                      decoration: formBoxDecoration,
                      padding: bodyPaddingAll,
                      child: FormBuilderTextField(
                        name: 'authNumber',
                        decoration:
                        StyleFormDecoration().getInputDecoration('인증번호'),
                        maxLength: 6,
                        keyboardType: TextInputType.text,
                        validator: FormBuilderValidators.compose([
                          FormBuilderValidators.required(
                              errorText: formErrorRequired),
                          FormBuilderValidators.minLength(6,
                              errorText: formErrorMinLength(6)),
                          FormBuilderValidators.maxLength(6,
                              errorText: formErrorMaxLength(6)),
                        ]),
                        enabled: _isAuthNumSend,
                      ),
                    )  // 인증번호가 전송이 됬을 때 보여준다.
                        : Container(), // 아무 것 도 하 지 않 았 을 때 보여준다.
                    _isAuthNumSend && !_isAuthNumSuccess
                        ? Container(
                      child: ComponentTextBtn(
                        '인증번호 확인',
                            () {
                          String phoneNumberText = _formKey
                              .currentState!.fields['phoneNumber']!.value;
                          String authNumberText =
                              _formKey.currentState!.fields['authNumber']!.value;

                          if (phoneNumberText.length == 13 &&
                              authNumberText.length == 6) {
                            AuthCheckRequest authCheckRequest =
                            AuthCheckRequest(
                              phoneNumberText,
                              authNumberText,
                            );
                            _doAuthCheck(authCheckRequest);

                          }
                        },
                        bgColor: colorSecondary,
                        borderColor: colorSecondary,
                      ),
                    )
                        : Container(),
                    const ComponentMarginVertical(),

                    _isAuthNumSend && _isAuthNumSuccess
                        ? Container(
                      child: ComponentTextBtn('아이디 확인', () => Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => const PageFindUsernameTwo())),),
                    )
                        : Container(),
                  ],
                ),
              ),
            ),
          ],
        ),

      ],
    );
  }
}
