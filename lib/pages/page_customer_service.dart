import 'package:daligo_customer_app/components/common/component_margin_vertical.dart';
import 'package:daligo_customer_app/components/common/component_text_btn.dart';
import 'package:daligo_customer_app/components/common/component_text_icon_full_btn.dart';
import 'package:daligo_customer_app/config/config_color.dart';
import 'package:daligo_customer_app/config/config_size.dart';
import 'package:daligo_customer_app/model/login_request.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';

class PageCustomerService extends StatefulWidget {
  const PageCustomerService({super.key, required this.title,
    this.isUseActionBtn1 = false,
    this.action1Icon,
    this.action1Callback,
    this.isUseActionBtn2 = false,
    this.action2Icon,
    this.action2Callback,});

  final String title;
  final bool isUseActionBtn1;
  final IconData? action1Icon;
  final VoidCallback? action1Callback;
  final bool isUseActionBtn2;
  final IconData? action2Icon;
  final VoidCallback? action2Callback;

  @override
  State<PageCustomerService> createState() => _PageCustomerServiceState();

  @override
  Size get preferredSize {
    return const Size.fromHeight(45);
  }
}

class _PageCustomerServiceState extends State<PageCustomerService> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
        centerTitle: false,
        // 타이틀(글자) 위치 가운데로 하겠니?
        automaticallyImplyLeading: false,
        // 타이틀(글자) 앞에 자동으로 뒤로가기버튼 같은거 달리는거 허용하겠니?
        leading: IconButton(icon: Icon(Icons.menu),
    onPressed: null,
    ),
    title: Text(widget.title),
    elevation: 1.0,
    // 앱바 아래 그림자처럼 보이는 것
    actions: [
    if (widget.isUseActionBtn1)
    IconButton(onPressed: widget.action1Callback,
    icon: Icon(widget.action1Icon)),
    if (widget.isUseActionBtn2)
    IconButton(onPressed: widget.action2Callback,
    icon: Icon(widget.action2Icon)),
    ],
    ),
      body: Container(
        padding: EdgeInsets.all(30),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text("상담하기",
                style: TextStyle(
                  fontWeight: FontWeight.w700,
                  fontSize: fontSizeSuper,
                ),
              ),
              SizedBox(
                height: 5,
              ),
              Divider(
                color: Colors.black,
              ),
              SizedBox(
                height: 5,
              ),
              Text("고객센터 상담시간",
              style: TextStyle(
                fontSize: fontSizeMid,
               ),
              ),
              SizedBox(
                height: 15,
              ),
              Text("전화상담 : 평일 09:00 - 18:00",
              style: TextStyle(
                fontSize: fontSizeBig,
                ),
              ),
              SizedBox(
                height: 5,
              ),
              Text("점심시간 12:00 - 14:00 / 주말 공휴일 휴무",
              style: TextStyle(
                fontSize: 10,
                color: colorDarkGray,
              ),
              ),
              SizedBox(
                height: 15,
              ),
              Text("채팅상담 : 평일 09:30 - 익일 01:00",
                style: TextStyle(
                  fontSize: fontSizeBig,
                ),
              ),
              SizedBox(
                height: 5,
              ),
              Text("점심시간 12:00 - 14:00 / 공휴일 휴무",
                style: TextStyle(
                  fontSize: 10,
                  color: colorDarkGray,
                ),
              ),
              const SizedBox(
                height: 30,
              ),
              Container(
                width: MediaQuery.of(context).size.width,
                child: ComponentTextIconFullBtn(Colors.amber, Colors.white, Icons.message, '채팅 상담하기', () {}),
              ),
              const SizedBox(
                height: 20,
              ),
              Container(
                width: MediaQuery.of(context).size.width,
                child: ComponentTextIconFullBtn(Colors.green, Colors.white, Icons.call, '전화 상담하기', () {}),
              ),
            ],
          ),
      ),
    );
  }
}