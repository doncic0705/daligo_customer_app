class MemberJoinRequest {
  String name;
  String username;
  String password;
  String passwordRe;
  String licenseNumber;
  String dateBirth;
  String phoneNumber;

  MemberJoinRequest(this.name, this.username, this.password, this.passwordRe, this.licenseNumber, this.dateBirth, this.phoneNumber);

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};

    data['name'] = name;
    data['username'] = username;
    data['password'] = password;
    data['passwordRe'] = passwordRe;
    data['licenseNumber'] = licenseNumber;
    data['dateBirth'] = dateBirth;
    data['phoneNumber'] = phoneNumber;

    return data;
  }
}