import 'package:daligo_customer_app/pages/page_customer_service.dart';
import 'package:daligo_customer_app/pages/page_my_information.dart';
import 'package:daligo_customer_app/pages/page_remain_price.dart';
import 'package:flutter/material.dart';

class ComponentDrawer extends StatelessWidget {
  const ComponentDrawer({super.key});

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        children: [
          UserAccountsDrawerHeader(
            currentAccountPicture: CircleAvatar(
                backgroundImage: AssetImage("assets/dog1.jpeg")
            ),
            accountName: Text('고달리'),
            accountEmail: Text('daligo@naver.com'),
            onDetailsPressed: () {},
            decoration: BoxDecoration(
              color: Colors.blue[200],
              borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(10.0),
                bottomRight: Radius.circular(10.0),
              ),
            ),
          ),
          ListTile(
            leading: Icon(Icons.home),
            iconColor: Colors.blue,
            focusColor: Colors.blue,
            title: Text('나의 정보'),
            onTap: () {Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => const PageMyInformation()));},
            trailing: Icon(Icons.navigate_next),
          ),
          ListTile(
            leading: Icon(Icons.account_circle),
            iconColor: Colors.blue,
            focusColor: Colors.blue,
            title: Text('이용기록'),
            onTap: () {},
            trailing: Icon(Icons.navigate_next),
          ),
          ListTile(
            leading: Icon(Icons.headset_mic),
            iconColor: Colors.blue,
            focusColor: Colors.blue,
            title: Text('고객센터'),
            onTap: () {
              Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (BuildContext context) => const PageCustomerService(title: "고객센터")), (route) => false);
            },
            trailing: Icon(Icons.navigate_next),
          ),
          ListTile(
            leading: Icon(Icons.money),
            iconColor: Colors.blue,
            focusColor: Colors.blue,
            title: Text('금액충전'),
            onTap: () {Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => const PageRemainPrice()));},
            trailing: Icon(Icons.navigate_next),
          ),

        ],
      ),
    );
  }
}
