import 'package:daligo_customer_app/functions/token_lib.dart';
import 'package:daligo_customer_app/model/common_result.dart';
import 'package:daligo_customer_app/model/find_username_request.dart';
import 'package:daligo_customer_app/model/login_find_password_request.dart';
import 'package:daligo_customer_app/model/member_join_request.dart';
import 'package:daligo_customer_app/model/update_password_request.dart';
import 'package:daligo_customer_app/model/update_request.dart';
import 'package:dio/dio.dart';
import 'package:daligo_customer_app/config/config_api.dart';
import 'package:daligo_customer_app/model/login_request.dart';
import 'package:daligo_customer_app/model/login_result.dart';

class RepoMember {
  Future<LoginResult> doLogin(LoginRequest loginRequest) async {
    const String baseUrl = '$apiUri/member/login/app/user';

    Dio dio = Dio();

    final response = await dio.post(
        baseUrl,
        data: loginRequest.toJson(),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );

    return LoginResult.fromJson(response.data);
  }

  Future<CommonResult> memberUpdate(UpdateRequest updateRequest) async {
    const String baseUrl = '$apiUri/member/member-update';

    String? token = await TokenLib.getMemberToken();

    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ${token!}';

    final response = await dio.put(
        baseUrl,
        data: updateRequest.toJson(),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );

    return CommonResult.fromJson(response.data);
  }
  Future<CommonResult> updatePassword(UpdatePasswordRequest updatePasswordRequest) async {
    const String baseUrl = '$apiUri/member/password';

    String? token = await TokenLib.getMemberToken();

    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ${token!}';

    final response = await dio.put(
        baseUrl,
        data: updatePasswordRequest.toJson(),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );

    return CommonResult.fromJson(response.data);
  }
  Future<CommonResult> findPassword(FindPasswordRequest findPasswordRequest) async {
    const String baseUrl = '$apiUri/member/find-password';

    Dio dio = Dio();

    final response = await dio.put(
        baseUrl,
        data: findPasswordRequest.toJson(),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );

    return CommonResult.fromJson(response.data);
  }
  Future<CommonResult> findUsername(FindUsernameRequest findUsernameRequest) async {
    const String baseUrl = '$apiUri/member/find-username';

    Dio dio = Dio();

    final response = await dio.put(
        baseUrl,
        data: findUsernameRequest.toJson(),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );

    return CommonResult.fromJson(response.data);
  }
  Future<CommonResult> memberJoin(MemberJoinRequest memberJoinRequest) async {
    const String baseUrl = '$apiUri/member/join';

    Dio dio = Dio();

    final response = await dio.post(
        baseUrl,
        data: memberJoinRequest.toJson(),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              print(status);
              return status == 200;
            }
        )
    );

    return CommonResult.fromJson(response.data);
  }
}