import 'package:daligo_customer_app/model/remain_amount_result.dart';
import 'package:dio/dio.dart';
import 'package:daligo_customer_app/config/config_api.dart';
import 'package:daligo_customer_app/functions/token_lib.dart';
import 'package:daligo_customer_app/model/common_result.dart';
import 'package:daligo_customer_app/model/remain_price_request.dart';

class RepoAmount {
  Future<CommonResult> doCharge(num price) async {
    const String baseUrl = '$apiUri/amount-charging/data?price={price}';

    String? token = await TokenLib.getMemberToken();

    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ${token!}';

    final response = await dio.post(
        baseUrl.replaceAll('{price}', price.toString()),
        // data: price.toJson(),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );

    return CommonResult.fromJson(response.data);
  }
  Future<RemainAmountResult> getRemainPrice() async {
    const String baseUrl = '$apiUri/remain-amount/remain-price';

    String? token = await TokenLib.getMemberToken();

    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ${token!}';

    final response = await dio.get(
        baseUrl,
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );
    return RemainAmountResult.fromJson(response.data);
  }
}